-- {   'aprs_type': 'position_weather',
--     'barometric_pressure': 10235,
--     'comment': '0.0dB',
--     'dstcall': 'OGNFNT',
--     'humidity': 0.5,
--     'latitude': 47.819,
--     'longitude': 13.105,
--     'name': 'FNT08DFBC',
--     'rainfall_1h': None,
--     'rainfall_24h': None,
--     'raw_message': 'FNT08DFBC>OGNFNT,qAS,guggenth:/135640h4749.14N/01306.30E_239/008g010t071h50b10235 '
--                    '0.0dB',
--     'receiver_name': 'guggenth',
--     'reference_timestamp': datetime.datetime(2023, 9, 19, 13, 56, 41, 690969),
--     'relay': None,
--     'symbolcode': '_',
--     'symboltable': '/',
--     'temperature': 21.666666666666668,
--     'timestamp': datetime.datetime(2023, 9, 19, 13, 56, 40),
--     'wind_direction': 239,
--     'wind_speed': 14.814601483188133,
--     'wind_speed_peak': 18.518251853985166}
-- 

BEGIN;
    DROP TABLE IF EXISTS gxaircom;
    CREATE TABLE gxaircom (
        id bigint AUTO_INCREMENT,
        barometric_pressure DOUBLE PRECISION,
        comment TEXT,
        dstcall TEXT,
        humidity TINYINT,
        latitude DOUBLE PRECISION,
        longitude DOUBLE PRECISION,
        name TEXT,
        rainfall_1h DOUBLE PRECISION,
        rainfall_24h DOUBLE PRECISION,
        receiver_name TEXT,
        reference_timestamp DATETIME,
        temperature DOUBLE PRECISION,
        data_timestamp DATETIME,
        wind_direction SMALLINT,
        wind_speed DOUBLE PRECISION,
        wind_speed_peak DOUBLE PRECISION,
        insert_timestamp BIGINT NOT NULL DEFAULT (UNIX_TIMESTAMP()),
        PRIMARY KEY(id),
        INDEX(insert_timestamp)
    );
    COMMIT;
