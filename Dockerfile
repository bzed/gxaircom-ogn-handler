FROM debian:stable-slim

ENV DEBIAN_FRONTEND noninteractive

RUN date '+%m%d%y' > .docker_build_date

RUN apt update -y && apt install -y eatmydata && eatmydata -- apt dist-upgrade -y

RUN eatmydata -- apt install -y \
        python3 \
        python3-pymysql \
        python3-yaml \
        python3-venv \
        python3-psycopg2 \
        python3-requests \
        python3-wheel \
        netcat-openbsd \
        curl

RUN eatmydata -- apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir -p /srv/GXAirCom_OGN_Handler/config
COPY GXAirCom_OGN_Handler.py /srv/GXAirCom_OGN_Handler
COPY requirements.txt /srv/GXAirCom_OGN_Handler

WORKDIR /srv/GXAirCom_OGN_Handler
RUN eatmydata -- python3 -m venv --system-site-packages .
RUN eatmydata -- /srv/GXAirCom_OGN_Handler/bin/pip3 install -r requirements.txt

CMD /srv/GXAirCom_OGN_Handler/bin/python3 \
    /srv/GXAirCom_OGN_Handler/GXAirCom_OGN_Handler.py \
    /srv/GXAirCom_OGN_Handler/config/config.yaml

