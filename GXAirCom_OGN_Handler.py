#!/usr/bin/env python3

"""
    (GxAircom) OGN (weather) data processor

    Copyright (C) 2023  Bernd Zeimetz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from ogn.client import AprsClient
from ogn.parser import parse, ParseError
import calendar
import datetime
import math
import pprint
import requests
import signal
import sys
import yaml

import multiprocessing as mp

PP = pprint.PrettyPrinter(indent=4)
pp = PP.pprint

CONFIG = {}
HANDLERS = {}
QUEUES = {}
AUTO_TEMP_FIX = {}
JOIN_STRING = "JOIN_QUEUE_NOW"
VERSION = "0.1"

def dew_point_fast(celsius, humidity):
    a = 17.271;
    b = 237.7;
    temp = (a * celsius) / (b + celsius) + math.log(humidity*0.01)
    Td = (b * temp) / (a - temp);
    return Td

def deg2f(celcius):
    return (celcius * 9/5) + 32;

def kmh2mph(kmh):
    return kmh / 1.609344;

def debug_handler(q, config):
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    while True:
        data = q.get()
        if type(data) == str and data == JOIN_STRING:
            break
        print('Received {aprs_type}: {raw_message}'.format(**data))
        pp(data)


def weather_underground_handler(q, config):
    import requests
    while True:
        data = q.get()
        if type(data) == str and data == JOIN_STRING:
            break

        proto = 'https'
        host = 'weatherstation.wunderground.com'
        location = "/weatherstation/updateweatherstation.php"
        url = "{}://{}{}".format(proto, host, location)

        if data['name'] not in config:
            print("Missing weather underground config for {}; ignoring! (this should never happen!)".format(data['name']))
            continue

        _id = config[data['name']]['id']
        _password = config[data['name']]['password']

        # https://support.weather.com/s/article/PWS-Upload-Protocol?language=en_US
        params = {
                'ID' : _id,
                'PASSWORD' : _password,
                'dateutc': data['timestamp'].replace(microsecond=0).isoformat().replace('T','+'),
                'softwaretype': "GXAirCom_OGN_Handler_v{}_for_{}".format(VERSION, data['name']),
                'action': 'updateraw',
        }

        haswd = lambda key: key in data and data[key] is not None
        wd = lambda key: data[key]
        f2s = lambda f: "{:.2f}".format(round(f, 2))

        if haswd('wind_direction'):
            params['winddir'] = f2s(wd('wind_direction'))
        if haswd('wind_speed'):
            params['windspeedmph'] = f2s(kmh2mph(wd('wind_speed')))
        if haswd('wind_speed_peak'):
            params['windgustmph'] = f2s(kmh2mph(wd('wind_speed_peak')))
        if haswd('temperature'):
            params['tempf'] = f2s(deg2f(wd('temperature')))
        if haswd('humidity'):
            humidity = wd('humidity') * 100
            params['humidity'] = humidity
            if haswd('temperature'):
                try:
                    dp = dew_point_fast(wd('temperature'), humidity)
                    params['dewptf'] = f2s(deg2f(dp))
                except Exception as e:
                    print("Failed to calculate dewpoint: {}".format(e))
                    pp(wd)
        if haswd('barometric_pressure'):
            params['baromin'] = f2s(data['barometric_pressure']/10 * 0.029529983071445)
        if haswd('rainfall_1h'):
                params['rainin'] = f2s(wd('rainfall_1h')/25.4)
        if haswd('rainfall_24h'):
                params['dailyrainin'] = f2s(wd('rainfall_1h')/25.4)

        p = '&'.join([ "{}={}".format(k, v) for k, v in params.items()])
        try:
            r = requests.get("{}?{}".format(url, p))
            if r.status_code != 200:
                print("WU Upload failed: {} {}".format(r.status_code, r.text))
                print("{}?{}".format(url, p))
        except Exception as e:
            print("WU Upload failed: {}".format(e))

def carbon_handler(q, config):
    if 'interval' in config:
        raise Exception('configuring interval in the carbon connection is not supported')
    import graphyte
    graphyte.init(**config['connection'])
    while True:
        data = q.get()
        if type(data) == str and data == JOIN_STRING:
            break
        #wanted_tags = [ 'receiver_name', 'latitude', 'longitude' ]
        data_fields = [
                'barometric_pressure', 'humidity',
                'rainfall_1h', 'rainfall_24h',
                'temperature',
                'wind_direction', 'wind_speed', 'wind_speed_peak',
                'latitude', 'longitude'
                ]
        #tags = {}
        #for tag in wanted_tags:
        #    if tag in data:
        #        tags[tag] = data[tag]
        #    else:
        #        print("tag {} missing:  {}".format(tag, str(data)))
        timestamp = calendar.timegm(data['timestamp'].utctimetuple())
        for field in data_fields:
            if field not in data or data[field] == None:
                continue
            if field == 'barometric_pressure':
                data[field] = data[field] / 10 # convert to hPa
            metric = "{}.{}".format(data['name'], field)
            graphyte.send(
                    metric,
                    data[field],
                    timestamp = timestamp,
                    #tags = tags
                    )

def mysql_handler(q, config):
    import pymysql
    signal.signal(signal.SIGINT, signal.SIG_IGN)

    conn = None
    while True:
        data = q.get()
        if type(data) == str and data == JOIN_STRING:
            break
        if data['aprs_type'] != 'position_weather':
            continue
        try:
            if not conn:
                conn = pymysql.connect(**config['connection'])
                conn.autocommit = False
            conn.begin()
            cursor = conn.cursor()
            table = config['table']
            insert = """
                insert into {} (
                    barometric_pressure,
                    comment,
                    dstcall,
                    humidity,
                    latitude,
                    longitude,
                    name,
                    rainfall_1h,
                    rainfall_24h,
                    receiver_name,
                    reference_timestamp,
                    temperature,
                    data_timestamp,
                    wind_direction,
                    wind_speed,
                    wind_speed_peak)
                VALUES(
                    %s,%s,%s,%s,%s,%s,%s,%s,
                    %s,%s,%s,%s,%s,%s,%s,%s
                    );
            """.format(table)

            cursor.execute(
                    insert, (
                    data['barometric_pressure']/10,
                    data['comment'],
                    data['dstcall'],
                    int(100 * data['humidity']),
                    data['latitude'],
                    data['longitude'],
                    data['name'],
                    data['rainfall_1h'],
                    data['rainfall_24h'],
                    data['receiver_name'],
                    data['reference_timestamp'],
                    data['temperature'],
                    data['timestamp'],
                    data['wind_direction'],
                    data['wind_speed'],
                    data['wind_speed_peak'],
                    ))
            cursor.close()
            conn.commit()
            if 'max_age' in config:
                max_age = config['max_age']
                conn.begin()
                cursor = conn.cursor()
                cursor.execute('delete from {} where insert_timestamp <= (UNIX_TIMESTAMP() - %s)'.format(table), max_age)
                cursor.close()
                conn.commit()
        except Exception as e:
            conn = None
            print("Exception in mysql handler: {}".format(e))
    if conn:
        conn.close()


def ogn_auto_temperature_fix_filter(beacon, config):
    #https://api.open-meteo.com/v1/forecast?latitude=47.819&longitude=13.104833333333334&current=temperature_2m
    receiver_name = beacon['receiver_name']
    longitude = beacon['longitude']
    latitude = beacon['latitude']

    if receiver_name not in AUTO_TEMP_FIX:
        try:
            r = requests.get("https://api.open-meteo.com/v1/forecast?latitude={}&longitude={}&current=temperature_2m".format(latitude, longitude))
            meteo = r.json()
            meteo_temp = meteo['current']['temperature_2m']
        except Exception as e:
            print("Failed to get weather data")
            print(e)
            return None
        beacon_temp = beacon['temperature']
        if (meteo_temp > (beacon_temp * 0.7)) and (meteo_temp < (beacon_temp * 1.3)):
            AUTO_TEMP_FIX[receiver_name] = 1
            print("New temperature correction factor: {} - 1".format(receiver_name))
        elif (meteo_temp > (0.5 * beacon_temp * 0.7)) and (meteo_temp < (0.5 * beacon_temp * 1.3)):
            AUTO_TEMP_FIX[receiver_name] = 0.5
            print("New temperature correction factor: {} - 0.5".format(receiver_name))
        else:
            print("Unknown temp difference: meteo: {}, beacon: {}".format(meteo_temp, beacon_temp))
            pp(beacon)
            return None
    beacon['temperature'] = beacon['temperature'] * AUTO_TEMP_FIX[receiver_name]
    return beacon


def ogn_temperature_fix_filter(beacon, config):
    for receiver_name, scale in config.items():
        if beacon['receiver_name'] == receiver_name:
            if 'temperature' in beacon:
                beacon['temperature'] = beacon['temperature'] * scale
    return beacon

def process_filters(beacon):
    for filter_name, config in CONFIG['filters'].items():
        if not beacon:
            return None
        filter_function = "{}_filter".format(filter_name)
        if filter_function not in globals():
            print("Filter function for {} not found.".format(filter_name))
            sys.exit(100)
        f = globals()[filter_function]
        beacon = f(beacon, config)

    return beacon

def process_beacon(raw_message):
    childs_alive()
    try:
        found = False
        found_station = None
        for station in CONFIG['stations'].keys():
            if raw_message.startswith("{}>".format(station)):
                found = True
                found_station = station
                break
        if not found:
            return
        beacon = parse(raw_message)
        if 'receiver_name' in CONFIG['stations'][found_station]:
            receiver_name = CONFIG['stations'][found_station]['receiver_name']
            if type(receiver_name) != list:
                receiver_name = [receiver_name]
            found = False
            for r in receiver_name:
                if r == beacon['receiver_name']:
                    found = True
                    break
            if not found:
                return
        if 'filters' in CONFIG:
            beacon = process_filters(beacon)
            if not beacon:
                return
        for handler_name in CONFIG['stations'][found_station]['handlers']:
            if QUEUES[handler_name].full():
                print("Queue {} full. This should never happen. Bye.".format(handler_name))
                sys.exit(100)
            QUEUES[handler_name].put(beacon)
        return
    except ParseError as e:
        print('Parser Error: {} from message {}'.format(e.message, raw_message))
    except NotImplementedError as e:
        print('{}: {}'.format(e, raw_message))
    except Exception as e:
        print('Other Exception: {}: {}'.format(e, raw_message))



def childs_alive():
    for p_name, p in HANDLERS.items():
        if not p.is_alive():
            print("Handler {} died, killing parent".format(p_name))
            sys.exit(1)

def setup_handlers():
    mp.set_start_method('spawn')
    for handler, handler_config in CONFIG['handlers'].items():
        handler_function = "{}_handler".format(handler)
        if handler_function not in globals():
            print("Handler function for {} not found.".format(handler))
            sys.exit(1)
        q = mp.Queue(200)
        p = mp.Process(
                target=globals()[handler_function],
                args=(q, handler_config)
                )
        p.start()
        HANDLERS[handler] = p
        QUEUES[handler] = q


def join_handlers():
    for handler in HANDLERS.keys():
        QUEUES[handler].put(JOIN_STRING)
    for handler, p in HANDLERS.items():
        p.join()


def main():
    keep_running = True

    setup_handlers()
    while keep_running:

        try:
            client = AprsClient(aprs_user='N1CALL')
            client.connect()
            client.run(callback=process_beacon, autoreconnect=True)
        except KeyboardInterrupt:
            print('\nStop ogn gateway')
            client.disconnect()
            keep_running = False
            join_handlers()
        except Exception as e:
            print('OGN Client exception: {}'.format(e))
    join_handlers()

if __name__ == '__main__':

    if len(sys.argv) < 2:
        print("Usage: {} <configfile>".format(sys.argv[0]))
        sys.exit(1)

    with open(sys.argv[1], 'r') as config_file:
        CONFIG = yaml.safe_load(config_file)

    main()
